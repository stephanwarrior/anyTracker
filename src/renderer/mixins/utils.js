import i18next from 'i18next';

export default {
    methods: {
        t(key) {
            return i18next.t(key);
        }
    }
};
