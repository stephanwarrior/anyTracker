export default {
    computed: {
        entities() {
            return this.$store.state.entities;
        },
        hasEntities() {
            return this.entities && this.entities.length > 0;
        },
        editing() {
            return this.$store.state.entitiesEditing;
        }
    }
};
