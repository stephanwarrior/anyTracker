import os from 'os';
import path from 'path';
import i18next from 'i18next';
import LngDetector from 'i18next-browser-languagedetector';
import Vue from 'vue';
import VueMaterial from 'vue-material';

import store from '@/store/store';
import router from '@/router/router';
import db from '@/../utils/db-helper';

import App from '@/components/App/App.vue';

Vue.use(VueMaterial);

i18next.use(LngDetector)
    .init({
        lng: 'en', // TODO i18next language from preferences
        fallbackLng: 'en',
        debug: process.env.NODE_ENV !== 'production',
        resources: {
            en: require('../locales/en.json')
        }
    }, (err, t) => {
        if (err) {
            // eslint-disable-next-line
            console.error('error while initialising i18next ::: ' + JSON.stringify(err));
        }
        initDB();
    });

function initDB() {
    db.init({
        // TODO DB path and name from preferences
        dbPath: path.join(os.homedir(), 'Desktop'),
        dbName: 'bla.db'
    }, (err) => {
        if (err) {
            throw err;
        }
        initVue();
    });
}

function initVue() {
    global.vm = new Vue({
        components: { App },
        store,
        router,
        template: '<App />',
        created() {
            this.$store.dispatch('updateStoreFromDb');
        }
    }).$mount('#root');
}
