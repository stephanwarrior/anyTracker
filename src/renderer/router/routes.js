import EntitiesList from '@/components/Entities/EntitiesList';
import EditEntity from '@/components/Entities/EditEntity';

import ParametersList from '@/components/Parameters/ParametersList';
import AddParameterValue from '@/components/Parameters/AddParameterValue';
import ParameterChart from '@/components/Parameters/ParameterChart';

import Settings from '@/components/Shared/Settings';
import About from '@/components/Shared/About';

export default [
    { path: '/', component: EntitiesList },
    { path: '/:entityId/edit', component: EditEntity },

    { path: '/:entityId/params', component: ParametersList },
    { path: '/:entityId/:paramId/value', component: AddParameterValue },
    { path: '/:entityId/:paramId/chart', component: ParameterChart },

    { path: '/settings', component: Settings },
    { path: '/about', component: About }
];
