import Vue from 'vue';
import VueRouter from 'vue-router';

import routes from './routes';

Vue.use(VueRouter);

const router = new VueRouter({
    routes
});
router.beforeEach((to, from, next) => {
    // to and from are Route Object, next() must be called to resolve the hook}
    if (global.vm) {
        global.vm.$store.commit('setCurrentScreenName', to.fullPath.replace('/', '') || 'home');
        global.vm.$store.commit('setDrawerStatus', false);
    }
    next();
});

export default router;
