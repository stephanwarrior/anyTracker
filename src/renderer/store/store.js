import Vue from 'vue';
import Vuex from 'vuex';

import state from './state';
import mutations from './mutations';
import actions from './actions';

Vue.use(Vuex);

const storeOptions = {
    strict: process.env.NODE_ENV === 'dev',
    // application data
    state,
    // // mutations are synchronous
    mutations,
    // // actions are a-synchronous
    actions
};

export default new Vuex.Store(storeOptions);
export { storeOptions };
