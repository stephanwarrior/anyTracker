export default {
    openDrawer: false,
    entities: [],
    entitiesEditing: false,
    currentScreenName: '/home',
    currentEntityEditing: ''
};
