import Vue from 'vue';

export default {
    setDrawerStatus(state, status) {
        state.openDrawer = status;
    },
    updateEntitiesContents(state, docs) {
        state.entities = docs;
    },
    addEntity(state, entity) {
        Vue.set(state.entities, state.entities.length, entity);
    },
    setEntitiesEditingStatus(state, status) {
        state.entitiesEditing = status;
    },
    setCurrentScreenName(state, screen) {
        state.currentScreenName = screen;
    },
    selectEntityForEditing(state, id) {
        state.currentEntityEditing = id;
    }
};
