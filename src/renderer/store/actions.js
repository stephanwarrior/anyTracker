import db from '@/../utils/db-helper';

export default {
    updateStoreFromDb(context) {
        db.getAllContents((err, docs) => {
            // TODO deal with possible error
            context.commit('updateEntitiesContents', docs);
        });
    },
    addEntity(context, payload) {
        db.addEntity(payload, (err, newEntity) => {
            // TODO deal with possible error
            context.commit('addEntity', newEntity);
        });
    },
    editEntity(context, payload) {
        db.editEntity(payload.modEntity, payload.cb);
    },
    deleteEntity(context, payload) {
        db.deleteEntity(payload.id, (err, numRemoved) => {
            // TODO this is inefficient, find a better way to update the list afeter removing an entry
            context.dispatch('updateStoreFromDb');
        });
    }
};
