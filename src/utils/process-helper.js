// here we use module.exports because code isn't compiled yet
module.exports = {
    onUncaughtException(err) {
        // TODO here we could for example log stuff to a different (internal) Datastore, so that we can keep track of what happens
    },
    onUnhandledRejection(reason, promise) {
        // TODO here we could for example log stuff to a different (internal) Datastore, so that we can keep track of what happens
    }
};
