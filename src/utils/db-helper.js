import Datastore from 'nedb';
import path from 'path';

let db;

export default {
    init({ dbPath, dbName }, onLoadCb) {
        /* istanbul ignore else */
        if (!db) {
            const opts = {
                autoload: true
            };
            /* istanbul ignore else */
            if (dbPath && dbName) {
                opts.filename = path.join(dbPath, dbName);
            }
            /* istanbul ignore else */
            if (onLoadCb) {
                opts.onload = onLoadCb;
            }
            db = new Datastore(opts);
        }
    },
    tearDown() {
        db = null;
    },
    getAllContents(cb){
        db.find({}, (err, docs) => {
            /* istanbul ignore else */
            if (cb) {
                cb(err, docs);
            }
        });
    },
    addEntity(entity, cb) {
        // TODO find a better way to do this. Perhaps a factory?
        const entityToInsert = {
            name: entity.name || 'New Entity',
            'description': entity.description || 'A new entity',
            'createdAt': Date.now(),
            'updatedAt': Date.now(),
            'parameters': entity.parameters || []
        };
        db.insert(entityToInsert, (err, newEntity) => {
            /* istanbul ignore else */
            if (cb) {
                cb(err, newEntity);
            }
        });
    },
    getEntityById(id, cb) {
        db.findOne({ _id: id }, (err, doc) => {
            /* istanbul ignore else */
            if (cb) {
                cb(err, doc);
            }
        });
    },
    editEntity(modEntity, cb) {
        // TODO expand this. What happens when we edit an entity to add/remove a parameters, for example?
        db.update({ _id: modEntity._id }, modEntity, (err, numAffected, affectedDocuments, upsert) => {
            /* istanbul ignore else */
            if (cb) {
                cb(err, numAffected);
            }
        });
    },
    deleteEntity(id, cb) {
        db.remove({ _id: id }, (err, numRemoved) => {
            /* istanbul ignore else */
            if (cb) {
                cb(err, numRemoved);
            }
        });
    }
};













