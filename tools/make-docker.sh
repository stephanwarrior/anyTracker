#!/bin/bash

TOKEN=$1
VERSION=$2

echo
echo "======================================="
echo "building image..."
echo "======================================="
docker build --no-cache -t sonar-scanner .

echo
echo "======================================="
echo "applying tag to image..."
echo "======================================="
echo "use tag "$VERSION
docker tag sonar-scanner registry.gitlab.com/stephanwarrior/anytracker/sonar-scanner:$VERSION

echo
echo "======================================="
echo "login to GitLab registry..."
echo "======================================="
docker login -u stephanwarrior -p $TOKEN registry.gitlab.com

echo
echo "======================================="
echo "push image to registry..."
echo "======================================="
docker push registry.gitlab.com/stephanwarrior/anytracker/sonar-scanner:$VERSION

echo
echo "======================================="
echo "all done!"
echo "======================================="
echo
