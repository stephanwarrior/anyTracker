'use strict';

const path = require('path');
const merge = require('webpack-merge');
const webpack = require('webpack');

const baseConfig = require('../../build/config/webpack.renderer.config');
const projectRoot = path.resolve(__dirname, '../../src/renderer');

// Set BABEL_ENV to use proper preset config
process.env.BABEL_ENV = 'test';


const webpackConfig = merge(baseConfig, {
    devtool: '#inline-source-map',
    plugins: process.env.NODE_ENV !== 'debug' ? [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': '"testing"'
        })
    ] : []
});

// don't treat dependencies as externals
delete webpackConfig.entry;
delete webpackConfig.externals;
delete webpackConfig.output.libraryTarget;

// apply vue option to apply babel-loader on js
webpackConfig.module.rules
    .find(rule => rule.use.loader === 'vue-loader').use.options.loaders.js = 'babel-loader';

module.exports = config => {
    config.set({
        browsers: ['customElectron', 'debugUnit'],
        client: {
            useIframe: false,
            captureConsole: true,
            runInParent: true
        },
        colors: true,
        coverageReporter: {
            dir: '../../coverage',
            reporters: [{
                type: 'lcov',
                subdir: '.'
            }, {
                type: 'text-summary'
            }]
        },
        customLaunchers: {
            'customElectron': {
                base: 'Electron',
                flags: ['--show']
            },
            'debugUnit': {
                base: 'Electron',
                flags: ['--show', '--remote-debugging-port=9333']
            }
        },
        frameworks: ['mocha', 'chai'],
        files: ['./index.js'],
        preprocessors: {
            './index.js': ['webpack', 'sourcemap'],
        },
        reporters: ['spec', 'coverage'],
        singleRun: process.env.NODE_ENV !== 'debug',
        webpack: webpackConfig,
        webpackMiddleware: {
            noInfo: true
        }
    })
};
