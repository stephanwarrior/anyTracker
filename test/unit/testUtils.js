const storeOpts = {
    mutations: {
        setCurrentScreenName(state, name) {},
        toggleEntitiesEditing(state) {},
        selectEntityForEditing(state, id) {}
    },
    actions: {
        updateStoreFromDb(context) {},
        addEntity(context, payload) {}
    }
};
const storeState = {
    openDrawer: false,
    entities: [],
    entitiesEditing: false,
    currentScreenName: '',
    currentEntityEditing: ''
};

export default {
    getStoreOpts() {
        const opts = storeOpts;
        opts.state = JSON.parse(JSON.stringify(storeState));
        return opts;
    },
    getStoreState() {
        return JSON.parse(JSON.stringify(storeState));
    }
};
