import { expect } from 'chai';
import path from 'path';
import fs from 'fs';
import db from '@/../utils/db-helper';

const dbPath = __dirname;
const dbName = 'temp-test.db';

/**
 * NB. we use timeouts here and there because all NeDB operations are file-based, hence asynchronous
 */
describe('db-helper', () => {

    beforeEach(removeDb);
    after(removeDb);

    describe('init', () => {
        it('should initialize a database file at given path', (done) => {
            db.init({ dbPath, dbName }, (err) => {
                expect(fs.existsSync(path.join(dbPath, dbName))).to.be.true;
                done();
            });
        });

        it('should call the given callback when db is loaded', (done) => {
            db.init({ dbPath, dbName }, (err) => {
                expect(err).to.be.null;
                done();
            });
        });

        it('should use in-memory only database if not provided a path/name', (done) => {
            db.init({}, (err) => {
                expect(fs.existsSync(path.join(dbPath, dbName))).to.be.false;
                done();
            });
        });
    });

    describe('tearDown', () => {
        it('should initialize the database and then tear it down', (done) => {
            db.init({}, (err) => {
                db.tearDown();
                expect(() => db.addEntity()).to.throw();
                done();
            });
        });
    });

    describe('getAllContents', () => {
        it('should retrieve all contents of the database', (done) => {
            db.init({ dbPath, dbName }, (err) => {
                db.addEntity({}, (err, newEntity) => {
                    db.addEntity({}, (err, newEntity2) => {
                        db.getAllContents((err, docs) => {
                            expect(docs).to.be.a('array');
                            expect(docs.length).to.eql(2);
                            done();
                        });
                    });
                });
            });
        });
    });

    describe('addEntity', () => {
        it('should add an entity to the database', (done) => {
            db.init({ dbPath, dbName }, (err) => {
                db.addEntity({
                    name: 'a name',
                    description: 'a description'
                }, (err, newEntity) => {
                    expect(err).to.be.null;
                    expect(newEntity._id).to.be.a('string');
                    done();
                });
            });
        });

        it('should use defaults when adding an entity without name or description to the database', (done) => {
            db.init({ dbPath, dbName }, (err) => {
                db.addEntity({}, (err, newEntity) => {
                    expect(err).to.be.null;
                    expect(newEntity._id).to.be.a('string');
                    expect(newEntity.name).to.eql('New Entity');
                    expect(newEntity.description).to.eql('A new entity');
                    done();
                });
            });
        });
    });

    describe('getEntityById', () => {
        it('should retrieve the entity by its id', (done) => {
            db.init({ dbPath, dbName }, (err) => {
                db.addEntity({}, (err, newEntity) => {
                    db.addEntity({}, (err, newEntity2) => {
                        db.getEntityById(newEntity._id, (err, doc) => {
                            expect(doc).to.not.be.null;
                            expect(doc).to.eql(newEntity);
                            done();
                        });
                    });
                });
            });
        });
    });

    describe('editEntity', () => {
        it('should update name and description of some entity', (done) => {
            db.init({ dbPath, dbName }, (err) => {
                db.addEntity({}, (err, newEntity) => {
                    newEntity.name = 'updated name';
                    newEntity.description = 'updated description';
                    db.editEntity(newEntity, (err, num) => {
                        expect(num).to.eql(1);
                        db.getEntityById(newEntity._id, (err, doc) => {
                            expect(doc.name).to.eql('updated name');
                            expect(doc.description).to.eql('updated description');
                            done();
                        });
                    });
                });
            });
        });
    });

    describe('deleteEntity', () => {
        it('should remove an entity from the db', (done) => {
            db.init({ dbPath, dbName }, (err) => {
                db.addEntity({}, (err, newEntity) => {
                    db.deleteEntity(newEntity._id, (err, num) => {
                        expect(num).to.eql(1);
                        db.getEntityById(newEntity._id, (err, doc) => {
                            expect(doc).to.be.null;
                            done();
                        });
                    });
                });
            });
        });
    });
});

function removeDb() {
    try {
        db.tearDown();
        fs.unlinkSync(path.join(dbPath, dbName));
    } catch (e) {
        // ignore "file not found" error
        if (e.code !== 'ENOENT') {
            throw e;
        }
    }
}
