import proc from '@/../utils/process-helper';

describe('process-helper', () => {
    describe('onUncaughtException', () => {
        it('should do nothing, for now', () => {
            proc.onUncaughtException();
        });
    });

    describe('onUnhandledRejection', () => {
        it('should do nothing, for now', () => {
            proc.onUnhandledRejection();
        });
    });
});
