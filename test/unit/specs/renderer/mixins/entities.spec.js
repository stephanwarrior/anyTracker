import Vue from 'vue';
import Vuex from 'vuex';
import { mount } from 'avoriaz';
import { expect } from 'chai';

import utils from '../../../testUtils.js';

import mix from '@/mixins/entities.js';

Vue.use(Vuex);

const Fake = Vue.component('Fake', {
    mixins: [ mix ],
    render: (h) => h('div')
});

const opts = () => utils.getStoreOpts();

describe('entities-mixin', () => {

    describe('computed', () => {
        describe('entities', () => {
            it('should return empty entities list when it is such', () => {
                const store = new Vuex.Store(opts());
                const wrapper = mount(Fake, { store });
                const ent = wrapper.vm.entities;
                expect(ent).to.eql([]);
            });

            it('should return present entities list when it is set to something', () => {
                const storeOpts = opts();
                storeOpts.state.entities = [{
                    name: 'this is an entity'
                }];
                const store = new Vuex.Store(storeOpts);
                const wrapper = mount(Fake, { store });
                const ent = wrapper.vm.entities;
                expect(ent.length).to.eql(1);
                expect(ent[0]).to.eql({
                    name: 'this is an entity'
                });
            });
        });

        describe('hasEntities', () => {
            it('should say there is no entity', () => {
                const store = new Vuex.Store(opts());
                const wrapper = mount(Fake, { store });
                const ent = wrapper.vm.hasEntities;
                expect(ent).to.be.false;
            });
            it('should say there are entities', () => {
                const storeOpts = opts();
                storeOpts.state.entities = [{
                    name: 'this is an entity'
                }];
                const store = new Vuex.Store(storeOpts);
                const wrapper = mount(Fake, { store });
                const ent = wrapper.vm.hasEntities;
                expect(ent).to.be.true;
            });
        });

        describe('editing', () => {
            it('should correctly track the entities edit status', () => {
                const store = new Vuex.Store(opts());
                const wrapper = mount(Fake, { store });
                const edit = wrapper.vm.editing;
                expect(edit).to.be.false;
            });
        });
    });
});
