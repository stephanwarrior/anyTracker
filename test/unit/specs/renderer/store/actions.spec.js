import { expect } from 'chai';
import db from '@/../utils/db-helper';
import actions from '@/store/actions';

describe('actions', () => {

    before(() => {
        db.tearDown();
    });

    beforeEach(() => {
        db.init({});
    });

    afterEach(() => {
        db.tearDown();
    });

    describe('updateStoreFromDb', () => {
        it('should retrieve all data from the database and then call the store to udpate the state', (done) => {
            const newEntity = { name: 'a', description: 'b' };
            db.addEntity(newEntity, (err, ent) => {
                actions.updateStoreFromDb({
                    commit(type, payload) {
                        expect(type).to.eql('updateEntitiesContents');
                        expect(payload).to.be.a('array');
                        expect(payload.length).to.eql(1);
                        expect(payload[0]).to.eql(ent);
                        done();
                    }
                });
            });
        });
    });

    describe('addEntity', () => {
        it('should add an entity to the database and then call the store to update the state', (done) => {
            const newEntity = { name: 'a', description: 'b' };
            actions.addEntity({
                commit(type, payload) {
                    expect(type).to.eql('addEntity');
                    expect(payload._id).to.be.a('string');
                    expect(payload.name).to.eql(newEntity.name);
                    expect(payload.description).to.eql(newEntity.description);
                    done();
                }
            }, newEntity);
        });
    });

    describe('editEntity', () => {
        it('should call the store to update the entity values', (done) => {
            db.addEntity({}, (err, newEntity) => {
                newEntity.name = 'new updated name';
                actions.editEntity({}, {
                    modEntity: newEntity,
                    cb: (err, num) => {
                        expect(num).to.eql(1);
                        done();
                    }
                });
            });
        });
    });

    describe('deleteEntity', () => {
        it('should dispatch a call to update the store content', (done) => {
            db.addEntity({}, (err, newEntity) => {
                actions.deleteEntity({
                    dispatch(type) {
                        expect(type).to.eql('updateStoreFromDb');
                        db.getAllContents((err, docs) => {
                            expect(docs).to.eql([]);
                            done();
                        });
                    }
                }, {
                    id: newEntity._id
                });
            });
        });
    });
});
