import { expect } from 'chai';
import mutations from '@/store/mutations';

import utils from '../../../testUtils.js';

const getState = () => utils.getStoreState();

let state;

describe('mutations', () => {

    beforeEach(() => {
        state = getState();
    });

    describe('setDrawerStatus', () => {
        it('should update the navigation drawer opening status', () => {
            mutations.setDrawerStatus(state, true);
            expect(state.openDrawer).to.be.true;
            mutations.setDrawerStatus(state, false);
            expect(state.openDrawer).to.be.false;
        });
    });

    describe('updateEntitiesContents', () => {
        it('should replace entities list', () => {
            const docs = [1, 2].map((i) => {
                return {
                    _id: Date.now().toString(),
                    name: 'entity' + i,
                    description: 'description' + i
                };
            });
            mutations.updateEntitiesContents(state, docs);
            expect(state.entities).to.eql(docs);
        });
    });

    describe('addEntity', () => {
        it('should add an entity to the state', () => {
            const newEntity = { _id: 'hdsL2JLHLKJDsJOSKsDssd' };
            mutations.addEntity(state, newEntity);
            expect(state.entities.length).to.eql(1);
            expect(state.entities[0]).to.eql(newEntity);
        });
    });

    describe('setEntitiesEditingStatus', () => {
        it('should toggle flag tracking entities edit status', () => {
            mutations.setEntitiesEditingStatus(state, true);
            expect(state.entitiesEditing).to.be.true;
            mutations.setEntitiesEditingStatus(state, false);
            expect(state.entitiesEditing).to.be.false;
        });
    });

    describe('setCurrentScreenName', () => {
        it('should update the current screen name', () => {
            mutations.setCurrentScreenName(state, 'bogus');
            expect(state.currentScreenName).to.eql('bogus');
        });
    });

    describe('selectEntityForEditing', () => {
        it('should update the id of the entity currently selected for editing', () => {
            mutations.selectEntityForEditing(state, 'thisisanid');
            expect(state.currentEntityEditing).to.eql('thisisanid');
        });
    });
});
