import { mount } from 'avoriaz';
import { expect } from 'chai';
import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import VueMaterial from 'vue-material';

import utils from '../../../../testUtils';

import App from '@/components/App/App';
import { storeOptions } from '@/store/store';

Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(VueMaterial);

const router = new VueRouter();

describe('App', () => {

    beforeEach(() => {
        storeOptions.state = utils.getStoreState();
    });

    after(() => {
        storeOptions.state = utils.getStoreState();
    });

    describe('created', () => {
        it('should initialize router and currentScreen name', () => {
            const store = new Vuex.Store(storeOptions);
            mount(App, { store, router });
            expect(router.currentRoute.path).to.eql('/');
            expect(store.state.currentScreenName).to.eql('home');
        });
    });

    describe('methods', () => {
        describe('updateEntitiesEditingStatus', () => {
            it('should toggle editing status if no entities are present', () => {
                const store = new Vuex.Store(storeOptions);
                const wrapper = mount(App, { store, router });
                wrapper.vm.updateEntitiesEditingStatus();
                expect(wrapper.vm.editing).to.be.true;
                wrapper.vm.$store.commit('addEntity', { _id: 'new entity id'});
                wrapper.vm.updateEntitiesEditingStatus();
                expect(wrapper.vm.editing).to.be.false;
            });
        });
    });
});
