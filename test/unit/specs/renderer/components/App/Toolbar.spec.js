import { mount } from 'avoriaz';
import { expect } from 'chai';
import Vue from 'vue';
import Vuex from 'vuex';
import VueMaterial from 'vue-material';

import utils from '../../../../testUtils';

import Toolbar from '@/components/App/Toolbar';
import { storeOptions } from '@/store/store';

Vue.use(Vuex);
Vue.use(VueMaterial);

describe('Toolbar', () => {

    beforeEach(() => {
        storeOptions.state = utils.getStoreState();
    });

    after(() => {
        storeOptions.state = utils.getStoreState();
    });

    describe('computed', () => {
        describe('showEditIcon', () => {
            it('should be true if screen is not home and viceversa', () => {
                const store = new Vuex.Store(storeOptions);
                const wrapper = mount(Toolbar, { store });
                store.commit('setCurrentScreenName', 'home');
                expect(wrapper.vm.showEditIcon).to.be.true;
                store.commit('setCurrentScreenName', 'list');
                expect(wrapper.vm.showEditIcon).to.be.false;
            });
        });
    });

    describe('methods', () => {
        describe('editEntities', () => {
            it('should toggle entitiesEditing flag when called', () => {
                const store = new Vuex.Store(storeOptions);
                const wrapper = mount(Toolbar, { store });
                expect(wrapper.vm.editing).to.be.false;
                wrapper.vm.editEntities();
                expect(wrapper.vm.editing).to.be.true;
            });
        });
        describe('toggleMenu', () => {
            it('should toggle menu status', () => {
                const store = new Vuex.Store(storeOptions);
                const wrapper = mount(Toolbar, { store });
                expect(storeOptions.state.openDrawer).to.be.false;
                wrapper.vm.toggleMenu();
                expect(storeOptions.state.openDrawer).to.be.true;
                wrapper.vm.toggleMenu();
                expect(storeOptions.state.openDrawer).to.be.false;
            });
        });
    });
});
