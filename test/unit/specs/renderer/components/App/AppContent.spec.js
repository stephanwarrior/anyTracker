import { mount } from 'avoriaz';
import { expect } from 'chai';
import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import VueMaterial from 'vue-material';

import utils from '../../../../testUtils';

import AppContent from '@/components/App/AppContent';
import { storeOptions } from '@/store/store';

Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(VueMaterial);

const router = new VueRouter();

describe('AppContent', () => {

    beforeEach(() => {
        storeOptions.state = utils.getStoreState();
    });

    after(() => {
        storeOptions.state = utils.getStoreState();
    });

    describe('computed', () => {
        describe('drawer', () => {
            it('should be set to what we want', () => {
                const store = new Vuex.Store(storeOptions);
                const wrapper = mount(AppContent, { store, router });
                store.commit('setDrawerStatus', true);
                expect(wrapper.vm.drawer).to.be.true;
                store.commit('setDrawerStatus', false);
                expect(wrapper.vm.drawer).to.be.false;
            });
        });
    });
});
