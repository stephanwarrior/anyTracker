import { mount } from 'avoriaz';
import chai, { expect } from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import VueMaterial from 'vue-material';

import utils from '../../../../testUtils';

import NavMenu from '@/components/App/NavMenu';
import { storeOptions } from '@/store/store';

chai.use(sinonChai);

Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(VueMaterial);

const router = new VueRouter();

describe('NavMenu', () => {

    beforeEach(() => {
        storeOptions.state = utils.getStoreState();
    });

    after(() => {
        storeOptions.state = utils.getStoreState();
    });

    describe('computed', () => {
        describe('status', () => {
            it('should be set to what we want', () => {
                const store = new Vuex.Store(storeOptions);
                const wrapper = mount(NavMenu, { store, router });
                store.commit('setDrawerStatus', true);
                expect(wrapper.vm.status).to.be.true;
                store.commit('setDrawerStatus', false);
                expect(wrapper.vm.status).to.be.false;
            });
        });
    });

    describe('watch', () => {
        describe('status', () => {
            it('should open or close the menu depending on newStatus', (done) => {
                const store = new Vuex.Store(storeOptions);
                const wrapper = mount(NavMenu, { store, router });
                const openSpy = sinon.spy();
                const closeSpy = sinon.spy();
                wrapper.vm.$refs.menu.open = openSpy;
                wrapper.vm.$refs.menu.close = closeSpy;
                store.commit('setDrawerStatus', true);
                wrapper.vm.$nextTick(() => {
                    expect(openSpy).to.have.been.called;
                    expect(closeSpy).to.not.have.been.called;
                    store.commit('setDrawerStatus', false);
                    wrapper.vm.$nextTick(() => {
                        expect(closeSpy).to.have.been.called;
                        expect(closeSpy).to.have.been.calledOnce;
                        expect(openSpy).to.have.been.calledOnce;
                        done();
                    });
                });
            });
        });
    });
});
