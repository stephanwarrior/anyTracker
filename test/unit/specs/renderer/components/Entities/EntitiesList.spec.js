import { mount } from 'avoriaz';
import { expect } from 'chai';
import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import VueMaterial from 'vue-material';

import utils from '../../../../testUtils.js';

import EntitiesList from '@/components/Entities/EntitiesList';
import { storeOptions } from '@/store/store';

import consts from '@/../utils/consts';

Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(VueMaterial);

const router = new VueRouter();

describe('EntitiesList', () => {

    beforeEach(() => {
        storeOptions.state = utils.getStoreState();
    });

    after(() => {
        storeOptions.state = utils.getStoreState();
    });

    describe('methods', () => {
        describe('addEntity', () => {
            it('should change route to add a new entity', () => {
                const store = new Vuex.Store(storeOptions);
                const wrapper = mount(EntitiesList, { store, router });
                wrapper.vm.addEntity();
                expect(storeOptions.state.entitiesEditing).to.be.false;
                expect(router.currentRoute.path).to.eql('/' + consts.entityFakeId + '/edit');
            });
        });

        describe('editEntity', () => {
            it('should change route to edit the given entity', () => {
                const id = 'ekhciwuncjknoi2jnd23jn42n';
                const store = new Vuex.Store(storeOptions);
                const wrapper = mount(EntitiesList, { store, router });
                wrapper.vm.editEntity(id);
                expect(storeOptions.state.entitiesEditing).to.be.false;
                expect(storeOptions.state.currentEntityEditing).to.eql(id);
                expect(router.currentRoute.path).to.eql('/ekhciwuncjknoi2jnd23jn42n/edit');
            });
        });
    });
});
