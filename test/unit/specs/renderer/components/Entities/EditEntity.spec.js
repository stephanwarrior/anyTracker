import { mount, shallow } from 'avoriaz';
import chai, { expect } from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import VueMaterial from 'vue-material';

import router from '@/router/router';

import utils from '../../../../testUtils';

import EditEntity from '@/components/Entities/EditEntity';
import { storeOptions } from '@/store/store';
import db from '@/../utils/db-helper';
import consts from '@/../utils/consts';

chai.use(sinonChai);

Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(VueMaterial);

// const router = new VueRouter();

describe('EditEntity', () => {

    beforeEach(() => {
        storeOptions.state = utils.getStoreState();
    });

    afterEach(() => {
        db.tearDown();
    });

    after(() => {
        storeOptions.state = utils.getStoreState();
    });

    describe('created', () => {
        it('should do nothing if there is no entity currently being edited', (done) => {
            db.init({}, () => {
                router.push('/' + consts.entityFakeId + '/edit');
                const store = new Vuex.Store(storeOptions);
                const wrapper = shallow(EditEntity, { store, router });
                Vue.nextTick(() => {
                    expect(wrapper.vm.editing).to.be.false;
                    expect(wrapper.vm.entity).to.eql({ parameters: [] });
                    done();
                });
            });
        });

        it('should retrieve the entity currently being edited', (done) => {
            db.init({}, (err) => {
                db.addEntity({}, (err, newEntity) => {
                    storeOptions.state.currentEntityEditing = newEntity._id;
                    const store = new Vuex.Store(storeOptions);
                    router.push('/' + newEntity._id + '/edit');
                    const wrapper = shallow(EditEntity, {
                        store,
                        router
                    });
                    setTimeout(() => {
                        expect(wrapper.vm.editing).to.be.true;
                        expect(wrapper.vm.entity).to.eql(newEntity);
                        done();
                    }, 500);
                });
            });
        });
    });

    describe('beforeDestroy', () => {
        it('should deselect the entity to edit', (done) => {
            db.init({}, () => {
                storeOptions.state.currentEntityEditing = 'something';
                const store = new Vuex.Store(storeOptions);
                const wrapper = shallow(EditEntity, {
                    store,
                    router
                });
                wrapper.destroy();
                expect(storeOptions.state.currentEntityEditing).to.eql('');
                done();
            });
        });
    });

    describe('methods', () => {
        // describe('done', () => {
        //     it('should dispatch the entity update for the one currently being edited', (done) => {
        //         db.init({}, () => {
        //             const editSpy = sinon.spy();
        //             const oldEdit = storeOptions.actions.editEntity;
        //             storeOptions.actions.editEntity = editSpy;
        //             const store = new Vuex.Store(storeOptions);
        //             const wrapper = mount(EditEntity, {
        //                 store,
        //                 router
        //             });
        //             const ent = {
        //                 name: 'entity',
        //                 description: 'a new one'
        //             };
        //             wrapper.vm.entity = ent;

        //             wrapper.vm.editing = true;
        //             wrapper.vm.done();
        //             FIXME code has changed, hence this test is no longer valid
        //             expect(editSpy.getCall(0).args[1].modEntity).to.eql(ent);
        //             expect(router.currentRoute.path).to.eql('/');
        //             storeOptions.actions.editEntity = oldEdit;
        //             done();
        //         });
        //     });
        //     it('should dispatch the db update after all', (done) => {
        //         db.init({}, (err) => {
        //             const ent = {
        //                 name: 'entity',
        //                 description: 'a new one'
        //             };
        //             db.addEntity(ent, (err, entity) => {
        //                 const updateSpy = sinon.spy();
        //                 const oldUpdate = storeOptions.actions.updateStoreFromDb;
        //                 storeOptions.actions.updateStoreFromDb = updateSpy;
        //                 const store = new Vuex.Store(storeOptions);
        //                 const wrapper = shallow(EditEntity, {
        //                     store,
        //                     router
        //                 });
        //                 wrapper.vm.entity = entity;
        //                 wrapper.vm.editing = true;
        //                 wrapper.vm.done();
        //                 expect(router.currentRoute.path).to.eql('/');
        //                 setTimeout(() => {
        //                     expect(updateSpy).to.have.been.called;
        //                     storeOptions.actions.updateStoreFromDb = oldUpdate;
        //                     done();
        //                 }, 250);
        //             });
        //         });
        //     });
        //     it('should dispatch the entity addition to db', (done) => {
        //         db.init({}, () => {
        //             const addSpy = sinon.spy();
        //             const oldAdd = storeOptions.actions.addEntity;
        //             storeOptions.actions.addEntity = addSpy;
        //             const store = new Vuex.Store(storeOptions);
        //             const wrapper = shallow(EditEntity, {
        //                 store,
        //                 router
        //             });
        //             const ent = {
        //                 name: 'entity',
        //                 description: 'a new one'
        //             };
        //             wrapper.vm.entity = ent;
        //             wrapper.vm.done();
        //             expect(addSpy.getCall(0).args[1]).to.eql(ent);
        //             expect(router.currentRoute.path).to.eql('/');
        //             storeOptions.actions.addEntity = oldAdd;
        //             done();
        //         });
        //     });
        // });

        describe('deleteEntity', () => {
            it('should call the confirmation dialog to open', () => {
                const store = new Vuex.Store(storeOptions);
                const wrapper = shallow(EditEntity, {
                    store,
                    router
                });
                const openSpy = sinon.spy();
                wrapper.vm.$refs['confirmDialog'].open = openSpy;
                wrapper.vm.deleteEntity();
                expect(openSpy.called).to.be.true;
            });
        });

        describe('onCloseDeleteEntity', () => {
            it('should do nothing if the user cancel the operation', () => {
                const deleteSpy = sinon.spy();
                const oldDelete = storeOptions.actions.deleteEntity;
                storeOptions.actions.deleteEntity = deleteSpy;
                const store = new Vuex.Store(storeOptions);
                router.push('/');
                const wrapper = shallow(EditEntity, {
                    store,
                    router
                });
                wrapper.vm.onCloseDeleteEntity('cancel');
                expect(deleteSpy.called).to.be.false;
                expect(router.currentRoute.path).to.eql('/');
                storeOptions.actions.deleteEntity = oldDelete;
            });

            it('should dispatch an action to delete the entity and go back home if user confirms', () => {
                const deleteSpy = sinon.spy();
                const oldDelete = storeOptions.actions.deleteEntity;
                storeOptions.actions.deleteEntity = deleteSpy;
                const store = new Vuex.Store(storeOptions);
                router.push('something');
                const wrapper = shallow(EditEntity, {
                    store,
                    router
                });
                wrapper.vm.onCloseDeleteEntity('ok');
                expect(deleteSpy.calledOnce).to.be.true;
                expect(router.currentRoute.path).to.eql('/');
                storeOptions.actions.deleteEntity = oldDelete;
            });
        });

        describe('addParameter', () => {
            it('should add a parameter to the entity\'s parameters list', () => {
                const store = new Vuex.Store(storeOptions);
                const wrapper = shallow(EditEntity, {
                    store,
                    router
                });
                wrapper.vm.addParameter();
                expect(wrapper.vm.entity.parameters.length).to.eql(1);
                //FIXME this won't work if in EditEntity we use "this.t()"
                expect(wrapper.vm.entity.parameters[0]).to.have.property('icon', 'assessment');
                expect(wrapper.vm.entity.parameters[0]).to.have.property('name', '');
                expect(wrapper.vm.entity.parameters[0]).to.have.property('description', '');

            });
        });
    });
});
