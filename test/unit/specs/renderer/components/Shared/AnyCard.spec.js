import { mount } from 'avoriaz';
import { expect } from 'chai';
import Vue from 'vue';
import VueMaterial from 'vue-material';

import AnyCard from '@/components/Shared/AnyCard';

Vue.use(VueMaterial);

describe('AnyCard', () => {

    describe('computed', () => {
        describe('edit', () => {
            it('should be false when showing a entity', () => {
                const wrapper = mount(AnyCard, {
                    propsData: {
                        type: 'entity',
                        editing: false
                    }
                });
                expect(wrapper.vm.edit).to.be.false;
            });
            it('should be true when editing a entity', () => {
                const wrapper = mount(AnyCard, {
                    propsData: {
                        type: 'entity',
                        editing: true
                    }
                });
                expect(wrapper.vm.edit).to.be.true;
            });
            it('should be true when showing a parameter', () => {
                const wrapper = mount(AnyCard, {
                    propsData: {
                        type: 'parameter'
                    }
                });
                expect(wrapper.vm.edit).to.be.true;
            });
        });
    });
});
