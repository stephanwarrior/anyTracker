import electron from 'electron';
import { Application } from 'spectron';
import path from 'path';
import fs from 'fs';
import os from 'os';

const timeout = process.env.CI ? 30000 : 20000;

export default {
    after() {
        try {
            fs.unlinkSync(path.join(path.join(os.homedir(), 'Desktop'), 'bla.db'));
        } catch (e) {
            // ignore "file not found" error
            if (e.code !== 'ENOENT') {
                throw e;
            }
        }
    },
    afterEach() {
        this.timeout(timeout);
        if (this.app && this.app.isRunning()) {
            return this.app.stop();
        }
    },
    beforeEach() {
        this.timeout(timeout);
        this.app = new Application({
            path: electron,
            args: ['dist/electron/main.js'],
            startTimeout: timeout,
            waitTimeout: timeout
        });
        return this.app.start().then((app) => {
            return app.client.waitUntilWindowLoaded();
        });
    }
};
