import { expect } from 'chai';
import utils from '../utils';

describe('Launch', function () {
    beforeEach(utils.beforeEach);
    afterEach(utils.afterEach);
    after(utils.after);

    it('shows the proper application title', function () {
        return this.app.client.getTitle()
            .then((title) => {
                expect(title).to.equal('anytracker');
            });
    });

    it('shows the "add" FAB when db is empty and takes you to the "add entity" page when clicked', function () {
        return this.app.client.element('button.md-fab')
            .isVisible()
            .then(visible => {
                expect(visible).to.be.true;
            })
            .click('button.md-fab')
            .then(result => {
                return this.app.client.element('div.entity-info-container').isVisible();
            })
            .then(visible => {
                expect(visible).to.be.true;
            });
    });
});
