[![build status](https://gitlab.com/stephanwarrior/anyTracker/badges/master/build.svg)](https://gitlab.com/stephanwarrior/anyTracker/commits/master)
[![coverage report](https://gitlab.com/stephanwarrior/anyTracker/badges/master/coverage.svg)](https://stephanwarrior.gitlab.io/anyTracker/coverage/lcov-report/index.html)
[![quality gate](https://sonarcloud.io/api/badges/gate?key=stephanwarrior:anytracker)](https://sonarcloud.io/dashboard?id=stephanwarrior%3Aanytracker)

# anyTracker

The project consists of a simple desktop application to track values over time. The values being tracked belong to parameters, and these parameters are grouped into entities.

The idea is that we start with _entitites_, which are comprised of _parameters_. These parameters have values that change over time.

Imagine yourself as an entity, and your weight as a parameter. You get onto the scales every day and record its values. Another parameters of yours could be whether you played your saxophone or not, or for how long.

Then you've got your favorite tree in the garden (another entity) and you want to track its gist (parameter).

## Data types

Values should probably be typed, thus filtering down what kind of interaction you can have with them.

Say, yes/no, integer, decimal, etc... We could even add a measure of unit!

## Visualize all the things

Eventually just recording all the values serves little purpose if you can't see them and make sense of it. That is why every parameter should have a visualisation page.

Different types of values probably have different visual styles, but you understand how it works: track values and then see them on a chart.

## Planned Features

- adding/removing/editing entities
- adding/removing/editing entities' parameters
- visualize parameters values on a chart
- add a virtual line to charts corresponding to the progressive sum of the values
- grouping of entities (say, group your wife, your kids and you under "family")
- thresholds on values (say, low, good, high bands)

