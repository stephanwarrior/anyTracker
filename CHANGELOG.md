# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/) and this project will adhere to [Semantic Versioning](http://semver.org/spec/v2.0.0.html) once all the major functionalities will be included.

Up till then we will only make PATCH increments.

## [Unreleased]
### Added
- Parameters section in "add/edit entity" page

## [0.0.7] - 2017-10-21
### Added
- Added [i18n](https://www.i18next.com/#) system.

### Changed
- Refactored code to make it more modular.
- Changed a few mutations to make them more predictable (e.g. no "toggle", but direct set of value).
- Added test coverage to get to 100%.
- Updated dependencies to latest versions.

## [0.0.6] - 2017-10-14
### Changed
- Moved development and build system to [electron-vue](https://github.com/SimulatedGREG/electron-vue).
- Updated CI configuration and Sonar exclusions.

## [0.0.5] - 2017-09-18
### Added
- Add Entity page. This doesn't include parameters list yet.

## [0.0.4] - 2017-08-30
### Added
- Entities list page

## [0.0.3] - 2017-08-27
### Added
- Material Design
- Navigation Drawer
- Database & Store

## [0.0.2] - 2017-08-19
### Added
- Vue.js and mock base components
- Vuex and [NeDB](https://github.com/louischatriot/nedb) for Database
- Unit and E2E tests
- Utilities for Docker and Sonar

## [0.0.1] - 2017-07-02
### Added
- Initialised project
- CI configuration files

[Unreleased]: https://gitlab.com/stephanwarrior/anyTracker/compare/v0.0.7...HEAD
[0.0.7]: https://gitlab.com/stephanwarrior/anyTracker/compare/v0.0.6...v0.0.7
[0.0.6]: https://gitlab.com/stephanwarrior/anyTracker/compare/v0.0.5...v0.0.6
[0.0.5]: https://gitlab.com/stephanwarrior/anyTracker/compare/v0.0.4...v0.0.5
[0.0.4]: https://gitlab.com/stephanwarrior/anyTracker/compare/v0.0.3...v0.0.4
[0.0.3]: https://gitlab.com/stephanwarrior/anyTracker/compare/v0.0.2...v0.0.3
[0.0.2]: https://gitlab.com/stephanwarrior/anyTracker/compare/v0.0.1...v0.0.2
[0.0.1]: https://gitlab.com/stephanwarrior/anyTracker/compare/6492b00...v0.0.1
